---
title: 19. INVENTION, INTELLECTUAL PROPERTY AND INCOME
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---

## INVENTION, INTELLECTUAL PROPERTY AND INCOME

During this class of Fab Academy, we learned about Intellectual Property and different licenses. These new inputs made me think differently and showed me a broad new world about registering my service.
First of all I would like to clarify the difference between **patent** and **license** and then also explain further about Intellectual Property.
- A **patent** *gives its owner the legal right to exclude others from making, using, selling, and importing an invention for a limited period of years*.
- A **license** *is an official permission or permit to do, use, or own something. A license can be granted by a party to another party as an element of an agreement between those parties*.
Each of them is a different form of Intellectual Property (IP) rights protection.
- **Intellectual Property** is a category of property that includes intangible creations of the human intellect and it could be presented in 4 different types: Trade Secrets / Trademarks / Copyrights / Patents.

The information we were able to learn during this session was very interesting and useful to place our intervention in the present world.
Also it was related to the information that we have already acquired during the last workshop of MDEF, EMERGENT BUSINESS MODEL, where they made us think about the levels of **IMPACT** that our project may have in the actual scenario:

![]({{site.baseurl}}/IMPACT19.jpg)

From my point of view, this project doesn’t have to be the answer to all the issues related to participatory design. Instead it could have some kind of impact either as a *Product/Service, or Develop a Capacity, or Develop Knowledge, or Develop a Movement/Behavior, either Develop an Infrastructure or Promote a Legislative Change*.
Taking this perspective as a starting point, I was somehow relieved as regards my project because I can frame my intervention in another existing context: being a part of another and bigger system, where I can offer a good service or improve some of the already existing services.
The last but not least important input from this workshop was the introduction of the **Pentagrowth: The five levels of exponential growth**:

![]({{site.baseurl}}/PENTA19.jpg)

From my perspective, the most interesting topics of this scheme were to *Empower the Users* and to *Enable the Partners*.
On the first level, it was very clear to see how the *Users could become Producers*, and how this path could take my project to an exponential growth. My intervention's goal is to generate the appropriate environment for the users and help them to interact as well as to work with the capabilities that these users can offer.  
Another level of this scheme that surprised me and made me reconsider my intervention, was to *Enable the Partners* instead of generating a competition. To connect with different companies and to generate a decentralized ecosystem, it’s a new perspective and a new approach for encouraging the horizontal workflow. To compare myself with others who work on the same field and to understand the place that each one takes up in relation to the impact' map, has helped me to gain ground and to be able to make a difference.

Getting back to the license issue, I've found out that there were many types for each particular case. But when dealing with software and application, the spectrum focuses on these three types: **GNU/GPL General Public License - MIT Software Licensing - Creative Commons Software Licensing**

-Source: https://www.monitis.com/blog/software-licensing-types-explained/-

These *software licenses* have been created to protect the copyright of the software and to restrict the ways in which the end user can use it. Many of these restrictions deal with software duplication or any type software alterations whatsoever. (i.e. editing it’s code)


-	**GNU/GPL General Public License**
General Public License is the most widely used type of free software license. This allows the end user of the program to retain the rights of the free software definition while using *copyleft* to make sure that the freedom of the license is protected even if the work is modified.
*Copyleft* is the opposite of a copyright which restricts the software recipient. *Copyleft* grants right to the recipient and makes sure that those rights cannot be taken away.

-	**MIT Software Licensing**
The MIT Software Licensing was created at the Massachusetts Institute of Technology. It is known as a *permissive free software license*, which means that it permits the recipient to reuse the software if all of the copies of the licensed software come with a copy of the MIT License' original terms. This proprietary software may incorporate software under the MIT License but it retains its proprietary nature.

-	**Creative Commons Software Licensing**
The term “Creative Commons” software licensing comes from the name of a non-profit organization. The Creative Commons organization is dedicated to expanding the range of creative works which are available for other users to build upon legally and then share. They have actually released several different copy-right licenses which are known as the Creative Commons licenses, which are all available to the public free of charge.
This is used for cases where the copyright owner is not seeking commercial gain from the software.

![]({{site.baseurl}}/SCALELICENSE.png)

Taking into account these options, I decided to research even further about CC licensing, since the nature of my software is non-commercial
There is a really friendly and understandable way of finding the perfect license, through [licensing-flowchart]( http://creativecommons.org.au/content/licensing-flowchart.pdf)

I follow the instructions and I found out that the right license for my project would be **Attribution Noncommercial-Share Alike license**

![]({{site.baseurl}}/LICENSE.png)

This license allows other users remix, tweak, and build upon your work non-commercially, as long as users credit you and license their new creations under identical terms.

-[Source](https://creativecommons.org/licenses/?lang=en)-

I’ve chosen this kind of license because my project in particular has to be contextualized for each new project since I will need and ask the users *distributing and copying my context* to do so.
- **ATTRIBUTION**: The application is able to shift the methodology of participatory design to digital interfaces and for that, each community will have different necessities and projects to approach. So for *changing the project and adapting its content*, certainly I would give permission in order to encompass more communities and more public designs.
- **NON-COMMERCIAL**: This project's aim is to be a useful tool for designers, developers and public entities, and its purpose is to give the users raw and non-manipulated information about the community and citizens involved. It’s a new way of making participatory design more accessible to everyone and to take into account what people likes or prefers regarding a public space.
- **SHARE ALIKE**: *This license allows other users remix, tweak, and build upon your work, as long as they credit you and license their new creations under identical terms*. This, in my view, is the only way to make sure about the IP: that I have the credit and also that the final product doesn’t get distorted and has the same attributes that those at the beginning of the process.

Also, I've found very interesting that CC licenses are the *three-layers design* that they arrange for the Legal Code, the Human Readable and the Machine Readable interpretation. This allows us to understand each of the stages taking part in the exchange process. These three layers of licenses jointly ensure that the spectrum of rights isn’t just a legal concept. It’s something that the creators of works can understand, as well as their users and even the Web itself.

Regarding **the next steps of my project**, I would have to work on two different phases.
One in relation to the inner structure of the application and the other with the community involved.
For each phase, I would need funds and also collaborators that will help me to achieve results faster, just because many of the collaborators have already been working either with the technologies involved or with the community.
For this reason, I would need to launch an open source model that allows permission to use the project’s source code, design documents and content. The idea is to have my collaborators available for the technological and the generating phase, not only by adding their knowledge, but also to modify and feed my project.
For this stage, the collaborators would be developers who already have worked within projects of the same category. This fact will allow me to share the possible algorithm with them and so, I will be able to analyze the data inputs and to give real time results.
[300.000kms](http://arturo.300000kms.net/#10)
[CitiBeats](https://citibeats.net/)

At the stage where I have to relate with the community matters, I will get in touch with local collaborators and with people that have already been working on public and participatory designs. During this phase, it would be very interesting to be able to work with real needs, real inputs and real citizens, in order to improve my project from working within a real scenario. My idea is not to create something that neighbors will need to learn how to use, but to process their ordinary data, and take into account their habits. Furthermore, the collaborators will give me feedback from the final results that I will deliver and also if these outputs are useful or not, as regards the design process.
[LACOL Cooperativa](http://www.lacol.coop/projectes/laborda/)

As you can see, this application will resemble a back-and-forth movement, between each community and each project, in order to expand and to be useful.
Regarding my project's communication, it could be improved by the association with other projects that already contacted me through the Web. One of them is **BarcelonaGame**, an app that invites tourists and local inhabitants to play and to get to know more about Barcelona. This in particular doesn’t relate to my intervention, but it is important to engage with similar innovations to help each other in relation to broadcasting and active users. The other project would be [SuperBarrio](http://superbarrio.iaac.net), designed and launched by former students of the Master in City & Technology at IaaC and with whom I could join the Alumni network and community to spread the project.

As for the funds that I will need to develop my application, and as I will be working for participatory designs in public spaces, I will submit my project to the public entities related to projects and communities in Barcelona.
**Barcelona Activa**, for example, has several PROGRAMS that carry out some actions for the creation and establishment of companies related to collective or specific activities. My project will be included in the Social Entrepreneurship program, within a Program of Pre-Incubation that allows to connect with other city's innovators, as big companies, universities, research centers and many start-ups.  
In order to address these several programs, it’s important to be included in the Barcelona Activa database and also to attend their several workshops in order to get involved with their internal structure that will technically advise me for the execution of my project.
