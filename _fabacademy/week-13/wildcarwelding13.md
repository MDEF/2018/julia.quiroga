---
title: 13. WILDCAR WEEK - WELDING
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---

## WELDING CLASS

For this assignment I decided to take a short class of welding.

## INTRO TO WELDING:
A welding is produced when two separate pieces of material are joined by using high heat to melt the parts together and then, form only one piece. Filler material is normally added to strengthen the joint.

Welding is a dependable, efficient and economic method for permanently joining similar metals. You can weld steel to steel or aluminum to aluminum, but you cannot weld steel to aluminum using traditional welding processes.

The number of different welding processes has grown in recent years. These processes differ greatly in the manner in which the heat and pressure are applied, and in the type of equipment used.
We learned about *gas metal arc welding* (GMAW), with carbon dioxide gas. This is very fast and easy to use, unlike popular *shielding metal arc welding* (SMAW) processes.
The first important difference between them is the cost of the equipment and then the type and number of tools that you will need for each method.
For the **SMAW** processes, also known as ‘stick’ welding, you will need a welding machine (a portable power supply, for example a generator) and electrodes. It is used primarily because of its low cost, flexibility, portability and versatility. Both the equipment and electrodes are low in cost and very simple.

![]({{site.baseurl}}/SMAW.png)

For **GMAW** processes you will need a welding machine (that regulates the voltage, the amount of gas and the speed the strand gets out of the gun), connected to a cylinder gas with carbon dioxide. You can avoid the electrodes because the welding gun will provide you with the material to heat, a continuous wire electrode. This method is extremely fast and also requires a lower level of operator skill than the other method.

![]({{site.baseurl}}/GMAW.png)
![]({{site.baseurl}}/POWERTEC.png)

For any method of welding you will have a *positive handle* and a *negative clamp*. The negative clamp has to be attached to any steel material, for example one of the pieces of metal that you are trying to join, and the positive handle is the welding gun (GMAW) or the electrode holder (SMAW)

![]({{site.baseurl}}/POSNEG.png)

Now that we explain a little bit of the two most popular processes for welding I’m going to detail some of the other tools and precautions that we need to have, while working in this field.
First of all, we have some special cloth to use while working, nothing that can heat too much or get inflamed. For that purpose, we will use a **special jacket**, **special gloves** and two different special kinds of **helmets**.

![]({{site.baseurl}}/JACKGLO.png)


The **gloves** have to be made of leather.
The **helmets** are of two kinds:
One for the operator that has an electrical screen that turns off when you are heating, so you can only see the flame, while working.
And the other one, for the assistants, that hasn’t got any electrical screen, but it only lets you see while the operator is heating, meanwhile you are “blind”

![]({{site.baseurl}}/HELMETS.png)

Another thing to have in mind before starting welding is to work in an isolated space, with the **adequate curtains**, so that somebody from outside the room that’s not wearing any helmet at all would not get contaminated. The last thing to take into account is to **warn everybody** before using the gun and getting closer to the metal pieces to weld.

![]({{site.baseurl}}/CURTAIN.png)

The *gas metal arc welding* heats by the contact between the strand and the pieces of metal, JUST LIKE THAT! So the only capability that you need to have is to understand, set and work with the right speed and power and a good and stable pulse. It’s a personal experience to get better in this field, by “feeling” the power and speed and testing one and again until you get confident with the gun.
Another important consideration for choosing the power is the *thickness* of the material you are trying to weld. In this case we test with a 5 mm thickness steel, so after testing and testing we were not able to cross over the piece to the other side.

Once you find your perfect values and you feel sure about them, you can test by working in *little circles*, heating the strand works good for your purpose.

![]({{site.baseurl}}/)
![]({{site.baseurl}}/DIFFWELD.png)

The *welding gun* has the function of feeding the operator with the strand. It also works with other tools for this task: **pliers** and a **metallic brush**.
- the **pliers** will be necessary to cut when you have a long strand getting out of the gun, you only need 1 cm sticking out to have special control over the gun and
- you will need the **metallic brush** once in a while, to clean the gun and the scraps

![]({{site.baseurl}}/CLEAN.jpg)

Some other instruments will be necessary while working in the welding processes:
- the **cutting metal tool**, for cutting the pieces and make them more malleable to work with, it would be also very important to polish the corners of the pieces before handling them, to avoid any accidents or sharp cuttings

![]({{site.baseurl}}/CUTTINGSTEEEL.png)

- the **magnet** to work perfect angels between the pieces, for example while working with corners or with pieces placed perpendicular one to another.

![]({{site.baseurl}}/ANGLE.png)

- the **grinder** to polish the final work and obtain a smooth surface. Also this stage will help you to see   if the pieces are well welded, without bubbles or discontinuity
- **handles** to grab the pieces and make them stay fixed to the workspace

The last part of the work will be done with the **grinder**, as I’ve already stated, to polish the surface and to obtain a homogeneous piece, where the joint doesn’t show.
For this last stage you will need as well some safety elements, as *plastic glasses*, *rubber gloves* and, of course, *handles* to grab the final piece.
The final work could be shown as this:

![]({{site.baseurl}}/FINALWORK.png)
