---
title: 08. COMPUTER-CONTROLLED MACHINING
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---
## CNC / COMPUTER-CONTROLLED MACHINING

This machine has many particularities that makes it one of the most complete machines of all. You can optimize your work since with a single machine you can achieve different jobs and also this machine does not take breaks. The **CNC machine** belongs to the family of CAM – Computer Aided Machining – and is a subtractive process, just like Laser Cut / CNC Milling / Vinyl Cutting.

It can have *3axis of control* (the long X, the bridge Y and the spindle Z) / *4axis control* (X, Y, Z and a rotated tool) / or *Robot Arms* with several axis.

![]({{site.baseurl}}/3AXIS.jpg)
![]({{site.baseurl}}/4AXIS.png)
![]({{site.baseurl}}/ROBOT.jpg)

As regards MOTORS, you have to consider the ROUTER, as the motor that can control the speed by hardware or the SPINDLE, where you can control the speed by software. This last one is more precise, sophisticated and expensive.

The other considerations for working with this kind of machine are the mills.
We have a 4 families of mills, they all cut and they all go in a clockwise direction:
1. *Straight – Cutting*
2. *Up Cut* (it gets out the material)
3. *Down Cut* (it compresses the material and you NEVER use it to make holes!)
4. *Compress* (it leaves material in the center, working first as an Up Cut and then as a Down Cut)
Also for each one of these types of mill you have to consider the END of the MILL:*Flat Mill* / *Rounded Mill*, to sand and smooth details / *V Cutter*, for shaping / *Spoilboard Surfacing Cutter*
The bigger the END of the MILL is, the faster you cut and with more FLUTES you create a smoother surface finish.

![]({{site.baseurl}}/MILLS.jpg)

The materials that you can use with this machine are *steel, aluminum, wood (solid wood or plywood), MDF (medium-density fibreboard), plastics, foams (expanded or extruded polystyrene) and wax (machine able wax)*

Some of the design’ tips that you can account are to **SCALE the drawing in mm, to CLEAN the design and to take it to 0.0 origin, to NEST the parts**, do not have fills just lines and to explode the text in polylines.
You have to consider some measurements for the **NESTING**: the Ø of the mill you are going to use x 2 + 10mm has to be the distance between the different shapes.

![]({{site.baseurl}}/NEST.jpg)

Another feature to study in your design is the **CLOSED ANGLES**, that the machine wouldn´t be able to mill. For this, at each CLOSE ANGLE you must draw a **T-BONE FILLET** (instead of 90°) with the Ø of the mill you are using, so that in that specific point the machine will read to do a hole.

![]({{site.baseurl}}/TBONE.jpg)

The software that you are going to use for these parameters and for establishing the different strategies is **RHINO CAM**
With this software you can decide the Ø of the mill, the kind of CUT, the END of the mill, which ACTIONS you want to do first (generally you choose first to *engrave* and then to *cut*) and you can set as many strategies as you want so the machine would understand the whole process.

![]({{site.baseurl}}/RHINOCAM.jpg)

After you have defined all these steps, you export the **GCODE** and go to the **VPANEL** of the machine. It works pretty similar as the VPANEL for the CNC Milling.
On one extreme of the bed, the *SETUP the 0.0.0* in X and Y axes is already set, but for Z axis you will need to do it manually by using a *Corner Finding Touch Plate system*, that allows the machine to calculate the distance to the real board.

![]({{site.baseurl}}/SETUP.PNG)
![]({{site.baseurl}}/ZPOINT.jpg)


After you make sure you *SETUP* the origin point in the 3 axes, you can go to the *PROGRAM Tab* and open the **GCODE**. When you open the code, you will see in a 3d graphic the different strategies you already set in **RHINO CAM**.

![]({{site.baseurl}}/PROGRAM.PNG)

The last thing to do before starting to cut is to connect the vacuum to the bridge and to turn it **ON**.
Once you *PLAY* the program, you have to be near the **STOP BUTTON** of the machine if anything wrong happens and you have to stop the operation.   

The whole process that we made took us 35minutes and was delivered in two layers with 4different strategies:
1.	*Engrave* some holes to pin up the board to the bed
2.	make some *Pockets* for the pieces inside
3.	cut from the *Inside*
4.	cut from the *Outside*
As we use the same Ø mill for every strategy we were allow to make 2 steps/layers: the 1st one to *Engrave*, stopped the machine and placed the screws and the 2nd one, with all the other actions.
At the end we had our pieces all cut down, ready to sand them and to fit for our final project.

![]({{site.baseurl}}/FINALPROJECTCNC.jpg)
![]({{site.baseurl}}/FINALCNC.jpg)
![]({{site.baseurl}}/CNC.JPG)

![]({{site.baseurl}}/MOTO1.JPG)
![]({{site.baseurl}}/MOTO2.JPG)
![]({{site.baseurl}}/MOTO3.JPG)

[link to the file]({{site.baseurl}}/files/BATMOBILE_CNC.3dm)

This project was made with the collaboration of
[Vasiliki Simitopoulou](https://mdef.gitlab.io/vasiliki.simitopoulou/)
[Aleksandra Łukaszewska](https://mdef.gitlab.io/ola.lukaszewska/)
