---
title: 07. ELECTRONIKS DESIGN
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---
## DESIGN PROCESS FOR THE PC BOARD

For this assignment we have worked with a software called **EAGLE AutoDesk**. What design the traces and the components that we will need for our board. First, we have to think of what is the effect you'd like to produce on this board. We already had a base, but the assignment was to add a *button* and a *LED light*, with all the other components that this action demanded.
The first thing to do is to open the Software and to add the library that we are going to use for the rest of the design. [Library/Open library manager] to add a new library. In the label of *Use*, you *Browse* the fab.lbr library, then click *Use*. Next you create a *New Project* and a *New Schematic* [Projects/Projects folder/New Project/New/Schematic]
Once on the schematic screen, you start adding the different components with the **ADD PART** tool. As at the soldering stage you start by the most difficult and important component: the *microcontroller ATtiny 44 SOIC14*. You add the ground *GND* and the power supply *VCC* as well, and to connect them to your ATtiny 44 component you will use the **NET** tool. This tool is really easy to use by clicking on the screen and tracing your path until you reach to the component. From this starting point you continue adding the others components as you did before: *AVR ISP connector*, a *resonator*, several *resistors* and so on.
With the *AVR ISP* we implement another way to connect it with the other components. It's not done by a physical connection (using the NET tool as a line), but instead we use the **R2** tool to *Name* the net with the component that you want to connect. After naming the net, check out that the *Place Label* is selected, a pop-up window will open and you'll be asked if you want to connect both components, just click *Yes*. This will ensure that you have a real connection.  

![]({{site.baseurl}}/SCHEMATIC.png)

Some of these components are required due to the remaining components, and not because they have a particular function. For example, the *resonator* is a crystal with a capacitor; where you are using the crystal as a clock for the microcontroller.
Then by adding a *LED 1206fab* you have to considered also a *resistor (R 499)* for each light, as well as with the *button/SWITCH (R 10K)*. So this is the way the PCBoard starts to complicate and fulfill itself.
At this point is when some basics notes from electronics start to be really useful: the RESISTOR, in the case of the led light, is essential to control the current through the led. And the RESISTOR, in the case of the switch, is a pull up resistor that indicates the direction of the current.
It’s important to take into account that the connections have to be right and must have some logic and a safe path: coming from the power supply and getting to the ground.
We have been working on a **SCHEMATIC** platform, where you add the components you need and you connect them with each other to make them worked. Before changing the platform to the **BOARD** you can CHECK the connections between all the components with **ERC** Tool (electrical check) [Tools/ERC] This will show you the *errors* in your design.

![]({{site.baseurl}}/ERC.png)

When we **GENERATE/SWITCH to board**, a pop-up window will appear and you'll be asked: *Create from schematic?*, just click *Yes*.  

![]({{site.baseurl}}/CREATES.png)

On the actual BOARD the first thing we have to do is to place all the components into our sheet with the **MOVE** tool. The first component that we move, will be the *ATtiny 44*. Then we select **RATSNEST** tool and select everything, to optimize the wires. This tool optimizes the length of the wires making shorter pcb traces.
Afterwards, we start to route our traces with the **ROUTE** tool, following the connections that we have already made in our schematic platform. Here it is important to decide and to switch the width of our traces. If we are going to mill the board, the traces should have a width of 16, but if we are going to vinyl cut, the width has to be 6.
With the ROUTE tool you can select between *Ignore Obstacles* or *Walkaround Obstacles*. This selection can be very useful in some cases to draw your traces between the components. Also you can change the **GRID** size of the board to allow for finer control.

![]({{site.baseurl}}/GRID.png)

Here again it is important to observe the connections and to try to track them in the best and neat way possible. Another useful tool is the **RIPUP** tool to delete any route you have made and you'd want to re-draw.
As regards the schematic stage, we also have the option to CHECK your tracks between the components with **DRC** Tool (design check) [Tools/DRC]
One last thing that can happened in this phase is that one track has to cross another track. Here the best way to solve this issue, if there isn´t another path to follow, is to ADD a JUMPER or a *RES-USFAB* (resistor 0 Ω) as a bridge at this intersection. Eagle wouldn’t allow this action on the BOARD environment, so I need to ADD and to CONNECT the resistor in the schematic screen. Once this is done, it will appear in the board.

![]({{site.baseurl}}/BOARD.png)

To **EXPORT** the files for milling to the machine, you have to consider two different layers: the one with TRACES and another with the INTERIOR. For this matter, you have to export images in .png format, with 1000 dpi, in monochrome style [File/Export/Image] and turning on in each case different layers [**LAYER SETTINGS**] For the TRACES image you have to turn on the *Top* and *Dimension* layers and for INTERIOR you have to only show the *Dimension* one.

![]({{site.baseurl}}/LAYERS.png)
![]({{site.baseurl}}/EXPORT.png)


![]({{site.baseurl}}/TRACES.png)
#### TRACES LAYER

![]({{site.baseurl}}/OUTLINE.png)
#### OUTLINE LAYER

[link to the file]({{site.baseurl}}/files/week 5 MILLING.brd)
[link to the file]({{site.baseurl}}/files/week 5 MILLING.sch)

![]({{site.baseurl}}/HERO.jpg)
#### HERO SHOT 
