---
title: 03. COMPUTER DESIGN
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---

As regards the programs to use for your 2D or 3D design, I suppose that they are related to the use you have been giving to them and how comfortable you are with the tools. I believe that there are as many tools as designers, and that each person feels more comfortable with some of these tools rather than others.
In my case, I´ve been working for several years with **Photoshop** and **Illustrator**, when I had to face 2D designs. I have to admit that my first experience with Illustrator was a bit distrustful, but once I get the logic and purpose of the program, a huge spectrum of possibilities opened up to me. These two programs helped me to approach any modification I needed to make on my works, each one with differences and scales, but being both part of the Suite of Adobe, makes it easier also. Issues like different formats or updating were not hard when working with them in combination.

When we faced 3D designs I started with **rhino** and still I'm amazed about the quality and definition of some designs. Although I'm an architect and the scale in which you work in this program isn't the best, in my case, it is a friendly way to design. Once I get into the platform, from the shortcuts, the menu and also the icons of the different tools make a whole enjoyable experience. It's also very useful that, particularly on the shortcuts, it maintains the same ones as **AutOCad**. This similarity between these different platforms makes the global design an easy going production.

#RHINO  
Let’s mention some topics before beginning that can help you in the global workshop.
You can decide in which *UNITS* you would like to work, so you can have some kind of idea about the scale you are managing. I always try to work in millimeters.
Also you can decide about the basic *Grid*, how many lines, the space between them and if you want to see it in every Viewport.
Another useful tools you can find in the bottom of the screen are: *OSNAP* (the same as in AutoCad, that shows you several important intersections between lines or surfaces) or *GUMBALL*, which I find very useful for making the basic actions as move up/down/left/right, stretch and rotate.

My first actions in Rhino were very simple:
For this first stage I preferred to work in the Top *VIEWPORT* with the Wireframe filter.  

1.	I started creating two shapes with the command *Circle* and with the command *Polyline*. You can draw your circle from the center, radius or from the diameter. I chose to draw it from the diameter (58mm)
![]({{site.baseurl}}/1CURVES.png)

2.	After I had my two closed geometries, I drew an *Arc/ start, end, point on arc* between two points, the middle point of my circle and one extreme of my rectangle. I decided to *Insert (more) control points* to mold my arc and give it a different silhouette. These control points enable you to disassemble a geometry or curve and to modify it in a more freely way
![]({{site.baseurl}}/2CONTROL.png)

3.	I create two surfaces with *Surface from Planar Curves* from my first shapes, the circle and the rectangle and I used the command *Mirror* to copy and reflect the open curve into the right side.
![]({{site.baseurl}}/3SURFACE.png)

4.	I create a close curve between the edges of the circle and the rectangle with the arc curves already modified. These *Join* curves let me create another surface with *Surface from Planar Curves*
![]({{site.baseurl}}/4MIRROR.png)

5.	I used *Mirror* again to copy and reflected the shape into the upper side. I add two closed surfaces in both sides of the circle, to complete the final shape (*Polyline/Surface from Planar Curves*)
![]({{site.baseurl}}/5MIRROR.png)

Now I have four different surfaces to work with: circle, rectangles and the surfaces generated between the edges and arcs.
For the next stages, and in relation to pre-visualization, I preferred to work in the Perspective *VIEWPORT* with the Shaded filter to understand the final design.

6.	*EXTRUDE surface* I used this command to extrude at different heights the surfaces. The circle is the highest in relation with the others (18mm)
![]({{site.baseurl}}/6EXTRUDE.png)

7.	I chose the action to *Cap Planar Holes* to pre¬-visualized my final solid in a close geometry
![]({{site.baseurl}}/7CAPPLANAR.png)

As regards different solids, there are some key tools you need to know and use. These are the *BOOLEAN actions (boolean union, boolean difference, boolean intersection, boolean split)*. My best way to work them through is by making simple solids and to understand how each operates. It is VERY IMPORTANT to pay attention to the instructions provided for each action in the *Command window* above.

8. (In TOP VIEW) I decided to *Move* the upper rectangle above and to connect it to the first rectangle.  For this action I used a *BOOLEAN ACTION/Boolean union* to merge both polysurfaces and make it one.
![]({{site.baseurl}}/8BOOLEAN.png)

9.	Finally I drew a close surface over the circle (*Polyline/Surface from Planar Curves*): a new rectangle
![]({{site.baseurl}}/9SURFACE.png)

10.	(In PERSPECTIVE VIEW) I *Extrude* this rectangle (1mm) inside the polysurface of the circle and I used the *Trim* action to lower this shape from the superior face of the circle
![]({{site.baseurl}}/10TRIM.png)


These are the basic commands I have been using for this week in order to approach my final project.
![]({{site.baseurl}}/RHINO.png)

[link to the file]({{site.baseurl}}/files/FINAL PROJECT.3dm)
