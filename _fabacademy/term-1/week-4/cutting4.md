---
title: 04. CONTROLLED CUTTING
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---

AS REGARDS my experience with the laser cut, I had to pass through several steps.
First of all, I decided to take a pavilion design from an old workshop, back in Argentina. The assignment for this week was to learn and experience laser cut printing, but also to work with the different joints. So I looked up into my past works and I found this example of pieces that can fit one into another without the use of glue and even more so, the stability of the whole set works.

![]({{site.baseurl}}/TROTEC.png)

This has not been my first time working with the laser cut machine, but it is important to remember some details for its appropriate use.
I decided to use 3mm-thick plywood, which was the parameter that I considered in the design for each piece to fit into the others. As the thickness of all pieces was 3mm, I had to calculate 3.1mm for the drawing so that the parts could fit smoothly in order not to force the structure.

![]({{site.baseurl}}/RHINO.jpg)

![]({{site.baseurl}}/RHINOM.jpg)
![]({{site.baseurl}}/JOINTS.png)


I redraw the design in **Rhinoceros**, so I didn´t have any format' issues while working with the programs that managed the laser cut. The only command that was left for last was the one to *resize* the design as the same dimension of the plywood (700x600mm) and to *define the lines to cut or to grave in different colors (blue for cutting and red for graving)*

![]({{site.baseurl}}/RHINOL.jpg)

Also there are several parameters in the printer drivers (**TROTEC JobControl X**) which you have to calibrate: *the power of the laser and the speed*. It depends on the material and its thickness, the values that you change or not. You can do a *previous test*, for understanding how this power and speed influence the laser and the cutting or graving in your chosen material.

At the end, before printing, you have to *focus* the lens. This action is performed whenever you change the material or when you start the process all over again. This focus will remain as such the whole process of printing.
![]({{site.baseurl}}/PROGRAM.jpg)

![]({{site.baseurl}}/LASER.jpg)

The project of the pavilion only works with cut pieces that fit one into another, so the graving parameters weren’t at all checked. In the cutting function, the power was checked, but it didn’t work out very well because I had to do manual work with the cutter to take out some of the pieces. I suppose it has to do with some details of the machine itself.

![]({{site.baseurl}}/NCUTTING.jpg)
![]({{site.baseurl}}/ASSEMBLY.jpg)

![]({{site.baseurl}}/DETAILS.png)
![]({{site.baseurl}}/FINISHl.png)

[link to the file]({{site.baseurl}}/files/PABELLON.3dm)

On the other hand, my experience with the vinyl cutter was very easy going. I worked the file in Illustrator and exported it as a .dxf extension. The machine is called Silhouette Cameo and has its own software (Silhouette Studio) to import, scale and print the files. In my case, I decided to print a PIG in white vinyl.

![]({{site.baseurl}}/VINYL.jpg)

The Silhouette Cameo works as a printing machine, but instead of having ink containers that drop ink over the paper, it has a blade with different depths that can be switched depending on the material we want to cut. We can cut cardboard, vinyl or even fabric, and for that purpose we can decide which depth is required. The software already has some preset values for different materials in the CUT SETTINGS section and it makes suggests what depth to use.

![]({{site.baseurl}}/CUTSETTINGS.png)

Another variable that you have to take into account for this machine is the paper size. As I was not using the cutting matt with this material I had to adjust the rollers to fit my vinyl, after unlocking the main roller shaft. Once I had it in position, I locked the shaft back in place and loaded my media by choosing the LOAD MEDIA option in the LSD screen of the Silhouette Cameo. Then I pressed ENTER and the machine set the vinyl.

![]({{site.baseurl}}/ROLLERS.jpg)
![]({{site.baseurl}}/LSD SCREEN.jpg)

From the software, and for this particular assignment, I chose (SEND TO SILHOUETTE / Current Settings / Change Settings) a depth of 2 with a speed 5, because it wasn´t such an intricate design. Once I defined my settings I run a test cut to check out these values.  
The test went well, so I decided to carry on with the same values. I double-checked that the image was in place in the cutting area of the software, but also I moved it a little bit to the center, so the piece already cut did not interfere. I loaded again the media manually to the machine, and from the software (SEND TO SILHOUETTE / Cut Page) I sent the file.
Once the machine finished, I pressed ENTER again to unload my material from the machine and peeled up my design. I used a spatula to help taking out the empty pieces and cleared the final figure. As the file was designed with different pieces that shaped the final figure, I covered it all with transfer tape to maintain the outline. For this stage I used a scraper board to avoid air bubbles. On the other hand, I cleaned up the surface where I wanted to stick my design with alcohol to make the sticker hold longer. The alcohol rapidly dry and I placed my figure over the clean surface, scraped it again and finally peeled out the tape. Here is the final result!

![]({{site.baseurl}}/PIG.jpg)

[link to the file]({{site.baseurl}}/files/VINYLCH.dxf)
