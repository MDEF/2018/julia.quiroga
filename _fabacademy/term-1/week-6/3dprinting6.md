---
title: 06. 3D PRINTING
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---

This week assignment was to 3d print and to 3d scan anything.
We've learned about the different 3d printers available in the market, their differences and features for distinctive uses.

All 3d printers work by adding material. In an economic and environmental order:
1. we have the most common and accessible one that is the **FDM** printer – Fused Deposition Modeling – that works with **PLA** a biodegradable and bioactive thermoplastic aliphatic polyester derived from renewable resources, such as corn starch, cassava roots, chips or starch.

![]({{site.baseurl}}/FDM.png)
![]({{site.baseurl}}/FDMmachine.jpg)

2. the second option will be **SLS** printer – Selective Laser Sintering – that works with polymer powder and additive glue, slide by slide. The difference with this printer, besides the cost and accessibility, is that prints in colors with a **CMYK** color model.

![]({{site.baseurl}}/SLS.png)
![]({{site.baseurl}}/SLSmachine.png)

3. the third and last option for printing is the most expensive and pollutant of all the printers, the **SLA** – Stereolithography – that works with resin and reacting to the light. This printer allows us to have fine details and smooth surface finish.

![]({{site.baseurl}}/SLA.png)
![]({{site.baseurl}}/SLAmachine.png)

The Software we used to import the file (in format .stl) and to control the parameters of the printer machine was **Ultimaker CURA**. This software allows us to work the different slides, the *quality* of the object in general, the *first layers*, the *shell* and the *top layers*, the *infill* and the *material*.

![]({{site.baseurl}}/CURA.png)

I will explain each one in order to understand the way the machine finally works.
For any object you will need some base and inner structure to stabilize the final product. That´s why you need to understand and to scale your object for this matter. Once you import the file into the software you can see your geometry and start working the different parameters required by the machine and the final product. The first layers will give you the base from where everything is kept. You don´t need to fill every part of the object if you have a good base, a dense mesh inside and a good top layer. For that matter it is also important to take into consideration the future utilization of this product, if it has to hold something or if you need it to be flexible. It will be very important to consider these characteristics on this previous stage.

![]({{site.baseurl}}/CURAINFILL.jpg)

Once you have worked out the parameters, you can send the file to the machine and start printing. The process that I had chosen for this assignment was the FDM printer and the object was a very little and simple one, without extra structure. So the process was very fast and the experience was great!

![]({{site.baseurl}}/3Dprint.jpg)
![]({{site.baseurl}}/FINAL3d.jpg)
