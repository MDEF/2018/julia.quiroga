---
title: 05. Navigating the Uncertainity
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---

#A MATTER OF DATA!  


I understand and believe that we live nowadays in a material world that has been designed taking into account our needs and desires.  So, I choose to see designers as those minds (and hands) that shape people’s demands.

Taking this as a starting point, it is important to look over the information we receive from the citizens who will live in the city that we design and deliver. Evolution and reality call for a change and require from us to start considering **thick data** that provides insights into the everyday emotional lives of the consumers. We must embrace the complexity that we have as human beings, and also as a society we cannot keep on clustering and asking for adaptations. The order of things has to change; *the system has to invite participation instead of demanding interaction (Design as Participation, Kevin Slavin, 2016)*
The equation is simple: If we are biological and socially complex beings, how it is that our cities keep on imposing only one pattern of configuration through the centuries? Our life styles must shape architecture and urbanism and not the other way round. At this time, and with huge technological advances that allow for new ways of living -including relations, habits, necessities and pleasures-, it is not conceivable that our design for a traditional floor plan hasn’t been readapted or even questioned. *We are forcing people to make use of some places they don’t get to use any more (Kitchenless City, Anna Puigjaner, 2011)*
![]({{site.baseurl}}/Untitled5.png)
#### *CLAUDE PARENT & PAUL VIRILIO // THE OBLIQUE FUNCTION (1970)*

*Today the technologies of communication, information exchange, and war, along with the economies of multinational capitalism and global commodity exchange, have produced a condition in which the urban site is no longer simply geographic… As Paul Virilio has pointed out, “the representation of the contemporary city is no longer determined by a ceremonial opening of gates, by a ritual of processions and parades, nor by a succession of streets and avenues. From now on architecture must deal with the advent of TECHNOLOGICAL SPACE-TIME.”*
*(Mapping the Unmappable; On Notation, Allen, Stan; Practice: Architecture, Technique, and Representation, 2000)*

So the first thing to do, in my opinion, is to reconsider what information we are taking into account, and how we are collecting, and also showing, such information. DATA is our principal source and I believe that we must design in the same way as we are gathering it. This information is already given to us, it is everywhere around; it is here whenever we make a decision, when we gather together, when we enjoy life and when we get upset, when we share experiences  and when we are interacting with reality. *Instead of using data just to become more efficient, we can use data to become more humans and to connect with ourselves and others at a deeper level (Data humanism: my manifesto for a new data world, Giorgia Lupi, 2015)*
![]({{site.baseurl}}/datahumanism.png)

It is a matter of collecting such data and to start considering it as inputs for our designs.
# We believe that every important interchange of information between people should always carry emotions and experiences and, also create knowledge, taking everything into account. (#DomesticDataStreamers)

The goal of my project is to improve the research facts using technology to obtain real data from citizens.
In this way we have Digital Literacy as a component, referring to an individual's ability to find, evaluate, produce and communicate clear information through digital platforms. We see things through INTERFACES, but we still don’t understand what this information is all about. I don’t leave aside from my hypothesis that we have to reeducate ourselves in how information is understood, but also that same information must be shown in a more transparent way. Climate change is a perfect example of how far away we are from understanding what is happening. If *we don’t know how climate change looks (José Luis de Vicente)*, it’s impossible for us to engage and to make some decisions or even changes in our everyday domestic habits.


[JF KIT HOUSE](https://www.youtube.com/watch?v=oTE0QtoUg7Q&feature=youtu.be "JF KIT HOUSE")

The example of JF Kit House brings out this matter: to understand and to be aware of what is our real footprint in this world that we want to change and live in. Information has to be delivered in a language that we all share and that we are capable to understand, a friendly data that is also consequent with its own purpose has to be provided to interact with citizens. Technologies, new apps and devices can show us information we can understand and experience with.

Let’s take language to get some perspective. Language is an interface that we use to communicate with others. It is a complex and organized interface that has been agreed by a group of people as a way to understand each other and resolve matters of coexistence. It is a convention between members of a society. Every language has its own history, and also it is used and (sometimes) adapted for each particular society. I have been analyzing the Chinese language, the ideograms are symbols that enclose many meanings through which you can find multiples connections and interpretations. This kind of communication embraces the dynamic and associative process that we as human beings experience in our everyday behavior. I find in this expression, as data visualization, the human condition of communication. So again, we are not so far from reeducating ourselves and achieving a natural process of language or visualizing information.

*Not only are we now realizing that there is still a substantial distance between the real potential that lies hidden in vast pools of data and the superficial imagery we often use to represent them, but most importantly, we realize that the first wave was successful in making others more familiar with new terms and visual languages (Data humanism: my manifesto for a new data world, Giorgia Lupi, 2015)*

Another example of this can be found in the Roget’s Thesaurus dictionary (1852) where the words are not defined for their definition in a lineal way, but instead for the relation that each word has with another, as synonymous and antonymous. *Based around a theory of how the human mind stores concepts (Fellbaum, 1998)* replicating a network that can show detailed relationships between words and their senses.

#### *HERE IS AN EXAMPLE FOR THE WORD "HAPPY"*
![]({{site.baseurl}}/Asset1.png)

If we observe nature we can also find other kind of representations that exemplify interactions with others or even relationships between our inner and structural parts. *Santiago Ramon y Cajal* in his drawings of *The Beautiful Brain*, illustrates how neurons connect with each other and shape different visual information that we can use to extract new (and many times not so evident) information, such as patterns or trends, or population of data or gaps in it. Bringing out the definition of fractals, by Mandelbrot, as a shape that reveals details at all scales.
![]({{site.baseurl}}/BeautifulBraincover.jpg)

Getting the big picture on your information, detecting how your ideas and information are related.  Use powerful visualization to discover information that would normally be overlooked, make connections and gain new understanding from your projects and content.

Facing this GAP, *this neuter space*, this break between the way we naturally process information (Natural Language Processing) and how we are forced to communicate, to adapt and also to live nowadays, this is where I want to work. To observe, to learn, and to include the data that we already have in our everyday behavior. Collecting information from this network, from our intrinsic complexity and using it to design HAPPY, participatory and engaged cities.
*In order to map this unmappable territory, the conventions of representation itself need to be rethought… New tools [must be developed] to work more effectively within the new immaterial networks… Architects need representational techniques that engage TIME AND CHANGE, shifting scales, mobile points of view, and multiple programs (Mapping the Unmappable; On Notation, Allen, Stan; Practice: Architecture, Technique, and Representation, 2000)*
Therefore the question consists now in how we collect the information that surrounds us.

I believe that we as a social specie have the same basic needs and most of the time the same desires, because the synapses of our brains, the chemistry of our blood and the statistical mass of our choices confirm that. Many philosophers, psychologists, brain scientists, artists and also politicians have been working in this issue, and they operated due to the complete and objective observation of human beings and their relationships. Mapping the city performing at real time, mapping the human dynamics, stimulating the citizens to play, as a *homo ludens (New Babylon, Constant, 1971)* in the free creation of his own life, is the starting point of this work.

#### *REPORTER app - Understanding through sampling (2012): is an application for tracking the things you care about. With a few randomly timed surveys each day, REPORTER app can illuminate aspects of your life that might be otherwise unmeasurable (by Nicholas Felton)*
![]({{site.baseurl}}/RECOVER.jpg)

Working without any established boundaries will helps us to construct new rules and new spaces, where the specifications will be given by what is needed instead of by its final prefiguration. This will allow for things to happen unexpectedly, new ways of interaction with the environment and also will allow for flexible, participatory and adaptive spaces to be shaped.

#### *IMAGINE Podcast, Episode 2: The Happiness Factors (by SPACE10/2018)*
![]({{site.baseurl}}/IMAGINE.jpg)


# **- READING LIST**
*Design as Participation, Kevin Slavin, 2016*

*Mapping the Unmappable; On Notation, Allen, Stan; Practice: Architecture, Technique, and Representation, 2000*

*Kitchenless City, Anna Puigjaner, 2011*

*New Babylon, Constant, 1971*

*Happy City, Charles Montgomery, 2013*

*The Death and Life of Great American Cities, Jane Jacobs, 1961*

“Com un gegant invisible. Can Batlló i les ciutats imaginaries” LaCol i Panóptica (2011)

*The Beautiful Brain: The Drawings of Santiago Ramon y Cajal, Eric Newman, Janet M. Dubinsky, and Larry W. Swanson, 2017*

*Stranger Than We Can Imagine: An Alternative History of the 20th Century, John Higgs, 2015*
