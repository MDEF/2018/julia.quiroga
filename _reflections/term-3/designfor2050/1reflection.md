---
title: DESIGN FOR 2050 - FINAL REFLECTION
period: 1-3 April 2019
date: 2018-10-06 12:00:00
published: true
---
## DESIGN FOR 2050

Designing for the future 2050 is not something easy to do. Somehow your imagination grows too much and projects a truly faraway future, even though 2050 is no more than 31 years from now.

**So how this ‘near’ future will be?**

We can imagine an utopian and positive future, but most of the time the scientists of the future have a more dystopian look at this topic. They believe that if things continue as they are, the future will not be a idyllic place; that we, as the human species, will have to make some adjustments as regards our manners and everyday routines if we want to make things better.
From this dystopian point of view, and in relation with the idea that our projects are designed to make things better, this kind of approach could help us to see which could be the worst scenario, to prevent our projects and desires from ending up badly (or, at least, trying to).
This projection helped us to portray ourselves and our projects in a black future (an apology of the Black Mirror' television series)
Most of us have been challenged by time, because, as I said before, 31 years is not that much time for developing some of our ideas and predictions. Nevertheless, the exercise of thinking about a future, near or faraway, was very interesting because our project is being put to the test, and so, it helps us to realize what could happen when you take the project to some extreme situations.

The assignment was to design a chapter for a future series, based on a dystopian future.
The series will be called Prismatic Minds, in relation to the reflection principle in the optics, where the prisms correct the image by placing it in the correct position.
This title has been decided among all of us, and then we will work in different groups to achieve the different chapters.

![]({{site.baseurl}}/PRISMA.png)

First of all, I would like to write about my personal reflection and my personal and individual chapter. Unlike my group final result, my future scenario wasn’t that good.
I started from a hopeful and revolutionary idea, from the theory of Situationism.
This concept is not an idea or project of my own, but my intervention works with many of the parameters that this movement had between the years 1957 and 1972, in Europe.
The emphasis of this theory was placed on concepts like *unitary urbanism* and *psychogeography*, understood, this last one, as "the study of the specific effects of the geographical environment (whether consciously organized or not) on the emotions and behavior of individuals".
In order to place my project in this future, I focused in the New Babylon city, designed by Constant Nieuwenhuys. This visionary architectural proposal defined that space as a psychic dimension cannot be separated from the space of action.
Nieuwenhuys' NEW BABYLON was to be a series of linked transformable structures, some of which were themselves the size of a small city - as a mega-structure. Perched above ground, Nieuwenhuys' mega-structures would literally leave the bourgeois metropolis below and would be populated by *homo ludens - man at play*. In New Babylon, the bourgeois shackles of work, family life, and civic responsibility would be discarded. The post-revolutionary individual would wander from one leisure environment to another in search of new sensations. Beholden to no one, he would sleep, eat, recreate, and procreate where and when he wanted. Self-fulfillment and self-satisfaction were New Babylon’s social goals.
New Babylon envisaged a society of total automation in which the need to work was replaced with a nomadic life of creative play, in which traditional architecture had disintegrated along with the social institutions that it had propped up. Every aspect of the environment could be controlled and reconfigured spontaneously. Social life became architectural play. Architecture became a flickering display of interacting desires.

![]({{site.baseurl}}/NEWBABYLON.png)

I decided to place my project in this futuristic environment because, in an exponential way, it works under the **participatory action of the inhabitant of this new city**, where they build their own spaces, responding to their needs and desires. It´s a horizontal approach for the design of public spaces, where no one has the right answer and final decision. So for my purpose, this scenario was perfect for running my intervention in a ‘near’ future.

But, as many extreme theories, designs and projections, these can work in a wrong way and the perfect future could become a nightmare. In my personal exercise, participation and collectivism there was stress, already. Anything could be designed and could change the environment. The Situationism theory had been brought to its full potential: a constant, mutable and horizontal public place where no one has the authority. The main characters were the *homo ludens* (man at play – the citizens) and the *neobabilions* (some type of AI machinery that continues to build and deconstruct for the society. Their objective will be to build immediate responses to the needs of citizens).
From this perspective it could be handled as something positive and promising, but What happens when freedom and playing is the only chance? When you are desperately looking for something stable? When do you no longer want to be the decision maker? Is there a way out? This was the inflection point of my chapter.
I realized after this exercise how some things and some good intentions could end up in a really bad situation. This possibility helped me think in many details I will need to take into account for managing the development of my intervention: that I will need to think in the worst scenario, by planning my intervention and present it to the community. It must have some kind of closed structure and also, keep on growing by the instant and tangible results. Speculative approaches need to be driven through a controlled path, to be opened for changes and re-adaptations as it keeps on growing.
It’s important to be realistic and have your feet on the ground while working, understanding (without judging) the principle characters of the intervention and how the dynamics work nowadays. From there, try to imagine their dynamics in the future. as well. Some fundamental aspects of my intervention is to be sincere about our own characteristics, our imperfections and our individualism; not because I will steer towards them, but to avoid them finally in its exponential scale, they will be the boundaries of my project.
One of the aspects of my intervention is to work with the individuals' characteristics, taking into account the imperfections and individualism within each person; not with the intention of boosting these characteristics but instead taking such as the limits or boundaries of my project, in order to avoid extremes.

In relation to our group chapter, the cluster of the groups was made by order of surnames. So, by the end, was very difficult to conceive a topic that can include all of our projects.
We finally put all our personal ideas on the table and chose one of them, the best that fit our desires at that moment. After this selection, we started specifying the structure of the chapter (*the Feature, the City where it will be developed, the Main Characters, the Inflection Point, the Conflict and the Title*)

[LOOKING TO SETTLE](https://player.vimeo.com/video/341754964)

On my personal experience the final result of the chapter, in relation to the narrative itself, didn’t’ show anything new, just because my intervention wasn’t even considered. But, as an exercise, in the end we profited from working in random groups, deciding and making a quick selection, and designing the structure of a chapter within a really short time.
Brainstorming and having quick and good results are something I would like to acknowledge. As an architect and designer, many times I feel lost while going from one idea to the other, without materializing any results. Finally ideas never grow, never face reality. This assignment helped me to materialize, to put hands on it and to come up with a preliminary result.
For my individual intervention, the key aspects are to face the public; to work with the people of the community for the real participatory design, leaving behind my expectations, my thoughts and even my desires; to work within the reality and with the reality, being able to change the course and to keep on developing my project through learning.
