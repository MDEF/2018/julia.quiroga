---
title: CHINA - SHENZHEN SUMMARY
period: 1-7 October 2018
date: 2018-10-06 12:00:00
published: true
---


# INSTANT CHINA - CONTRAST

![]({{site.baseurl}}/INSTANTCH.jpg)


I went to **Shenzhen** with the innocent idea that Chinese people will understand global language, or what I consider as such, the English language. Instead, I´ve found out that they do not really understand it and also they don’t care to learn English at all. So, at that point, it was when I began to open my eyes and to realize that there was a new language that could help me to communicate with them and to express somehow my basic necessities. Here it is when the digital language, with the platform of digital devices, it’s shown to me as the new way of communicating. *What would it be if these devices do not exist?*

I took several taxis, and the only way to explain to the driver where we were heading was with our cell phones. First the map, then the app translator, then again the map and so on. If I wanted to buy something and lower the price for example, something pretty usual in the local markets of electronics, the calculator or even a tablet were the perfect devices for writing down numbers and starting in some way the negotiation. So most of the time there was another component between the people I need to communicate something and myself. The interaction wasn’t between me and another person, but between me, a digital device and later between the digital device and the other. A conversation in triangle. A conversation where the rules weren’t the ones of the spoken or written language, but the rules of digital language.  

Another aspect that impressed me was the magnitude that China, as a country, does manage. We all have this idea about millions of people living in such a huge country and, that most of the things we use come from there (the label *MADE IN CHINA* is a recognizable label in many things we have in our homes).
But to face this reality is a different thing, to see and to really touch this quantity gives you another dimension of the whole.

Let’s start with the local market of electronics in **Huaqiang Bei**, where there are whole buildings, each with several floor plants, full of whatever you fancy in the electronic field. Starting almost from zero, from the tiny little component of a circuit to a finished computer with all the accessories you can imagine.

Another way to measure the scale are the huge and tall buildings of social housing, growing up as trees in every part of the country. You feel somehow as if the horizon is redrawing itself once and again through the new silhouette of the skyscrapers.

I discovered the last characteristic of this aspect in the factory’s, on our visit to **Dongguan**, the manufactory center. Here we were able to visit the complete chain of production and to witness the amount of people working and living there; the specific stages to manufacture each product and finally, the last step of shifting for the suppliers all over the world (*“MADE IN CHINA”*). The experience was quite contradictory, in the way that we have a communist structure working, each one on a specific stage of the production chain, being in charge of a particular machine and delivering to the next stage. All this scenery framed in a consumerist and capitalism reality.
It was the perfect contrast that characterized the city of **Shenzhen**.  

To finish my summary of this research trip, I would like to write down my impression of Shenzhen as a living factory. All the people living there is, somehow, destined to work and to construct something. The city has the amount of people, the amount of space, the amount of equipment and most important a political development incentive to do so. The people of Shenzhen can make almost everything, they have the matrices and the technicians that can adapt to any new design. But they are hungry for innovating ideas, for making the difference and for growing up in a creative quality. It is as if they were born to do but not to improve, because they don’t pay attention to progress, to improve the way they live or the way they are moving, even to expand their experience to the world. Issues as labor exploitation, conditions of house living and pollution are still a taboo in this “advanced” part of the world.
So again, contrast is presented as the main representative aspect of this young city.  

I left **Shenzhen** hoping that instead of striving for making more money, the search will focus in the balance and the reorganization of their priorities as part of a big and plentiful world.
